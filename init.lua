mobs_7dtc = {
	bmg_wind = { },
	timer = 0,
}

-- Load support for intllib.
mobs_7dtc.modname = minetest.get_current_modname()
mobs_7dtc.modpath = minetest.get_modpath(mobs_7dtc.modname) .. "/"
mobs_7dtc.intllib = minetest.get_translator and minetest.get_translator(mobs_7dtc.modname) or dofile(mobs_7dtc.modpath .. "intllib.lua")

dofile(mobs_7dtc.modpath .. "autosettings.lua")
dofile(mobs_7dtc.modpath .. "blood_fog.lua")
dofile(mobs_7dtc.modpath .. "mob_curse.lua")
dofile(mobs_7dtc.modpath .. "commands.lua") 
dofile(mobs_7dtc.modpath .. "spawn_v4.lua") -- O Curse não nasce sem esse!


print ("[MOBS_7DTC] Loaded!!!")
