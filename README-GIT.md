--------------------------------------------------------------------------
## INTRODUÇÃO AO GIT
--------------------------------------------------------------------------

01º Verifica a versão do GIT

````$ git version````
 
 Obs.: Minha versão é 1.9.1. Mas o tutorial é para versões 2.3.2.

02º Cria um Repositório Local (no PC):

````$ git init````

03º Indentificando o Usuário:

````
git config --global user.email "lunovox@disroot.org"
git config --global user.name "Lunovox"
git config --global core.editor "xed"
git config --global alias.ncommit 'commit -a --allow-empty-message -m ""'
git config --global alias.nlog 'log --oneline --decorate --all --graph'
````

04º Identificando arquivo que estão fora do projeto 

````$ git status````
 
05º criando arquivos vazio para versinamento:

````
$ echo a > a.txt
$ echo b > b.txt
````
 
06º Colocando os arquivo no "Stage Area" (tipo de conteiner):

````$ git add -A````
 
07º Colocando o "Stage Area"(conteiner) no repositório local (no PC):

````$ git commit -m "Adicionando a.txt e b.txt"````
 
08º Exibe o histório de commits:

````$ git log````
 
````
commit 9d6a01d5d40d6cc1f64a6a9dc72cbb5f48ea7ca6
Author: Lunovox <lunovox@disroot.org>
Date:   Sun Nov 18 05:03:52 2018 -0300

    Adicionando a.txt e b.txt
````

09º Fazendo sequencia de comando para adicionar um novo arquivo no repositório local (no PC):

````
 $ echo c > c.txt
 $ git status
 $ git add -A
 $ git commit -m "Adicionando 'c.txt' no repositório."
````
 
10º Alterando o conteudo de um arquivo:

````
 $ echo c2 > c.txt
 $ git status
 $ git add -A
 $ git commit -m "Modificando 'c.txt' no repositório."
````
 
 OBS.: Isso coloca o texto "c2" dentro do arquivo "c.txt" e poe no repositório.

11º Renomeando um arquivo:

````
 $ mv c.txt c2.txt
 $ git status
 $ git add -A
 $ git commit -m "Modificando 'c.txt' no repositório."
````
 
 OBS.: Isso renomeia  "c.txt" para "c2.txt" e poe no repositório local.
 
12º Verificando log de repositório com uma única linha

````$ git log --oneline````

13º Verificando lista de ramificações de projeto.

````$ git branch````

````
 > * master
````

 OBS.: se não ouver ramificações (branch) então exibir o ramo principal ("master").
  
13º Criando uma ramificação no projeto.

````
 $ git branch feature1
 $ git branch
````

>   feature1
> * master
 
 OBS.: Isso cria uma ramificação(branch) chamada "feature1". Note o que o ponteiro exibido pelo asterisco (*) ainda está no "master". 

14º Verificando log de repositório com os ponteiro de branch.

````$ git log --oneline --decorate````

 
> 7df2690 (HEAD, master, feature1) c2.txt
> 65d5693 Modificando 'c.txt' no repositório local.
> 0bf1959 Adicionando 'c.txt' ao projeto.
> 9d6a01d Adicionando a.txt e b.txt

OBS.: Note que "HEAD" está apontado antes de "master". Isso significa que ele esta pontado para o mesmo que está todo em um commit "7df2690".
 
15º Por o ponteiro (HEAD) sobre o branch "feature1":

````
 $ git checkout feature1
 $ git branch
````

> * feature1
>   master

16º Criando um arquivo na ramificação "feature1":

````
 $ echo d > d.txt
 $ git add -A
 $ git commit -m "Adicionando 'd.txt' no repositório."
 $ git log --oneline --decorate
````
 
> 93973db (HEAD, feature1) Adicionando 'd.txt' no repositório.
> 7df2690 (master) c2.txt
> 65d5693 Modificando 'c.txt' no repositório local.
> 0bf1959 Adicionando 'c.txt' ao projeto.
> 9d6a01d Adicionando a.txt e b.txt

 OBS.: Observe que o ponteiro (HEAD) está sobre o branch(remificação) "feature1" que por sua vez está sobre o commit "93973db". Enquanto o ramo principal ("mastes") está um commit anterior em "7df2690".
 
17º Retornando o ponteiro(HEAD) para o branch(ramificação) "master":

````
 $ git checkout master
 $ ls
````
 
> a.txt  b.txt  c2.txt
 
OBS.: Note que o arquivo "d.txt" sumiu. Isso ocorre porque o branch(ramo) "master" não possui o arquivo "d.txt" da mesma forma que o branch "feature1" possui.
  
18º Listar todos os arquivos escondidos em todos os branchs(ramos):

````$ git log --oneline --decorate --all````

 > 93973db (feature1) Adicionando 'd.txt' no repositório.
 > 7df2690 (HEAD, master) c2.txt
 > 65d5693 Modificando 'c.txt' no repositório local.
 > 0bf1959 Adicionando 'c.txt' ao projeto.
 > 9d6a01d Adicionando a.txt e b.txt
 
19º Mesclahndo(merge) o ramo(branch) "feature1" para dentro do ramo "master":

````
 $ git checkout master
 $ git merge feature1
````

> Updating 7df2690..93973db
> Fast-forward
>  d.txt | 1 +
>  1 file changed, 1 insertion(+)
>  create mode 100644 d.txt

````$ git log --oneline --decorate --all````

> 93973db (HEAD, master, feature1) Adicionando 'd.txt' no repositório.
> 7df2690 c2.txt
> 65d5693 Modificando 'c.txt' no repositório local.
> 0bf1959 Adicionando 'c.txt' ao projeto.
> 9d6a01d Adicionando a.txt e b.txt
 
OBS.: Note que agora tanto os ramos(branchs) "master" e "feature1" possuem o arquivo "d.txt". Note a mensagem "Fast-forward" que apenas adiciona ao "master" aos arquivos que lhe faltavam sem causar conflitos.

20º Apagando o branch(ramo) "feature1" que não é mais necessário.

````
$ git checkout master
$ git branch -d feature1
$ git log --oneline --decorate --all
````

> 93973db (HEAD, master) Adicionando 'd.txt' no repositório.
> 7df2690 c2.txt
> 65d5693 Modificando 'c.txt' no repositório local.
> 0bf1959 Adicionando 'c.txt' ao projeto.
> 9d6a01d Adicionando a.txt e b.txt

21º Criando uma divergência entre um ramo(branch) o tronco "master":

````
$ git branch feature2
$ git checkout feature2
````

ou só:

````
$ git checkout -b feature2
$ echo e > e.txt
$ git add -A
$ git commit -m "Adicionando 'e.txt' no repositório."
$ git checkout master
$ echo f > f.txt
$ git add -A
$ git commit -m "Adicionando 'f.txt' no repositório."
$ git checkout master
$ git merge feature2 -m "mesclando 'feature2' e 'master'."
````
>  'master'."
>  Merge made by the 'recursive' strategy.
>   e.txt | 1 +
>   1 file changed, 1 insertion(+)
>   create mode 100644 e.txt

````
$ git branch -d feature2
````
 
22º Lista commits graficamente:

````$ git log --oneline --decorate --all --graph````

> *   48ee213 (HEAD, master) mesclando 'feature2' e 'master'.
> |\  
> | * 9c64e31 Adicionando 'e.txt' no repositório.
> * | b8c7a8e Adicionando 'f.txt' no repositório.
> |/  
> * 93973db Adicionando 'd.txt' no repositório.
> * 7df2690 c2.txt
> * 65d5693 Modificando 'c.txt' no repositório local.
> * 0bf1959 Adicionando 'c.txt' ao projeto.
> * 9d6a01d Adicionando a.txt e b.txt

OBS.: Note que o arquivo "e.txt" está no branck(ramo) "feature2", enquanto o arquivo "f.txt" está no ramo principal (branch "master").

23º criando um comando/alias/atalho temporario para o "git log" gigante:

````
 $ alias git-log="git log --oneline --decorate --all --graph"
 $ alias git-revert="git reset --hard origin/master"
 $ alias git-up='git add *; read -p "Digite a mensagem: " msg; clear; git commit -m "$msg"; git push; clear; git status'
 $ git-log
````
 
OBS.: Note que agora basta digitar o comando "````gitlog````" ao invés "````git log --oneline --decorate --all --graph````"

24º Criando um conflito entre um ramo(branch) o tronco "master":

````
$ git checkout -b feature3
$ echo g2 > g.txt
$ git add -A
$ git commit -m "Alterando 'g.txt' com texto "g2" no repositório."
$ git checkout master
$ echo g1 > g.txt
$ git add -A
$ git commit -m "Alterando 'g.txt' com texto "g1" no repositório."
$ git merge feature3 -m "mesclando 'feature3' e 'master'."
````

> 'master'."
> Mesclagem automática de g.txt
> CONFLITO (adicionar/adicionar): conflito de mesclagem em g.txt
> Automatic merge failed; fix conflicts and then commit the result.

````$ cat g.txt````

> <<<<<<< HEAD
> g1
> =======
> g2
> >>>>>>> feature3

````$ nano g.txt````

> g2

````
$ git add -A
$ git commit -m "Resolvendo conflito de 'g.txt' no repositório."
$ git branch -d feature3
````

OBS.: Note que houve um conflito entre os arquivos "g.txt" do ramo(branch) "feature3" e do tronco "master".

--------------------------------------------------------------------------

25º Fazendo diff com o meld:

Instalando o programa [meld]: 
 * ````sudo apt-get install meld````

Configure o **difftool** para aceitar o programa meld: 
 * ````git config --global diff.tool meld````

Faça um difftool com todos os arquivos do repositório remoto: 
 * ````git difftool origin/master *````

O mesmo que o anterior só que sem questionar qual arquivo um por um:
 * ````git difftool -y origin/master *````

Simplificando o comando [difftool] com um alias:
 * ````alias git-diff="git difftool origin/master *"````



