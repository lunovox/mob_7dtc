mobs_7dtc.getRandomWelcome = function(playerTarget)
	if playerTarget ~= nil and playerTarget:is_player() then
		local playername = core.colorize("#FF8800", playerTarget:get_player_name())
		local msg = {
			("Retornei mais uma vez para este mundo para te judiar, %s!!!"):format(playername),
			("%s não me vencerá, pois não pode matar quem já esta morto!"):format(playername),
			("Voltei para arrancar a lingua de %s pelo sovaco!!!!"):format(playername),
			("Touxe um pão de Mefistófeles. Abra boquinha %s e diga: AAAAAAAAAAH!"):format(playername),
			("Agora e sua vez %s!"):format(playername),
			("Você não me escapa %s!"):format(playername),
			("A sua hora chegou %s!"):format(playername),
			("Vim para darmos um 'pequeno' passeio em Nether, %s!"):format(playername),
			("Vim buscar %s para transformá-lo no macaquinho do Mefistófeles!"):format(playername),
			("Judiarei de %s até a morte, e a morte, e a morte!"):format(playername),
			("Vou arrancar as tripas de %s para servir de como meu fio-dental!"):format(playername),
			("Você vai se arrepender por está andando no escuro, %s!"):format(playername),
			("Fiz um pacto com o Mefistófeles para trazer %s comigo para o Nether!!!"):format(playername),
		}
		return core.colorize("#FF00FF", "<MALDITO CURSE> ").."Ha! Ha! Ha! Ha! "..msg[math.random(1, #msg)]
	else
		return ""
	end
end

mobs_7dtc.putFire = function(posTarget)
	minetest.sound_play("sfx_bruxa_rizada", {pos=posTarget, max_hear_distance = mobs_7dtc.RaioDeCurse})
	mobs_7dtc.lightning.strike()
	--[[ 
	local posTarget2 = posTarget
	posTarget2.y = posTarget2.y + 1
	if posTarget.y > -10
		and minetest.get_node(posTarget).name == "air"
		and minetest.get_node(posTarget2).name == "air"
	then
		mobs_7dtc.lightning.strike(posTarget) --desabilitado por estar matando o jogador estantaneamente.
	else
		mobs_7dtc.lightning.strike()
	end
	--]]
end



mobs_7dtc.on_spawn = function(pos, node)
	if mobs_7dtc.isNowFogTime() then
		mobs_7dtc.RaioDeCurse = 500
		mobs_7dtc.playerTarget = nil
		mobs_7dtc.mobCurse = nil
		local objs = minetest.get_objects_inside_radius(pos, mobs_7dtc.RaioDeCurse)
		for _,myTarget in pairs(objs) do --Testa se ja ha algum Curse por perto
			
			if mobs_7dtc.playerTarget == nil then
				if myTarget~=nil and myTarget:is_player() then
					local playername = myTarget:get_player_name()
					local posPlayer = myTarget:get_pos()
					local lightPlayer = minetest.get_node_light(posPlayer, nil)
					mobs_7dtc.debug(playername.." => lightPlayer(".. minetest.pos_to_string(posPlayer) ..") => ["..dump(lightPlayer).."<=4]")
					if type(lightPlayer)=="number" and lightPlayer <= 4 then 
						mobs_7dtc.playerTarget = myTarget
					end 
				end
			end
			
			if mobs_7dtc.mobCurse == nil then
				if myTarget~=nil 
					and not myTarget:is_player() 
					and myTarget:get_luaentity() 
					and myTarget:get_luaentity().name 
					and myTarget:get_luaentity().name == "mobs_7dtc:curse" 
				then
					mobs_7dtc.mobCurse = myTarget:get_luaentity()
					mobs_7dtc.debug(mobs_7dtc.mobCurse.name.."[".. 
						minetest.pos_to_string(
							mobs_7dtc.mobCurse.object:get_pos()
						) .."]"
					)
				end
			end
			
			--verifica se o alvo foi achado antes de frear a varredura do mapa.
			if mobs_7dtc.playerTarget ~= nil 
				and mobs_7dtc.playerTarget:is_player()
			then
				--verifique se já existe um mob no mapa antes de frear a varredura do mapa.
				if mobs_7dtc.mobCurse ~= nil
					and type(mobs_7dtc.mobCurse.name) == "string"
					and mobs_7dtc.mobCurse.name == "mobs_7dtc:curse"
				then
					break
				end
			end
		end --end for
		
		--verifica mais uma vez se o jogador está online antes de colocar o mob.
		if mobs_7dtc.playerTarget ~= nil 
			and mobs_7dtc.playerTarget:is_player()
		then
			local posPlayerTarget = mobs_7dtc.playerTarget:get_pos() 
			posPlayerTarget.y = posPlayerTarget.y + 2
			--descobrir se o mob vai ser deslocado ou recriado
			if mobs_7dtc.mobCurse ~= nil
				and type(mobs_7dtc.mobCurse.name) == "string"
				and mobs_7dtc.mobCurse.name == "mobs_7dtc:curse"
				and mobs_7dtc.mobCurse.object:get_pos() ~= nil
			then
				local posMob = mobs_7dtc.mobCurse.object:get_pos()
				--mobs_7dtc.lightning.strike(posPlayerTarget) --desabilitado por estar matando o jogador instantaneamente.
				mobs_7dtc.mobCurse.object:set_pos(posPlayerTarget)
				mobs_7dtc.mobCurse.state = "attack" 
				mobs_7dtc.mobCurse.attack = mobs_7dtc.playerTarget
				mobs_7dtc.debug("deslocando mob: "..dump(posMob, "").." ==> "..minetest.pos_to_string(posPlayerTarget))
			else
				mobs_7dtc.putFire(posPlayerTarget)
				mobs_7dtc.mobCurse = minetest.add_entity(posPlayerTarget, "mobs_7dtc:curse", nil)
				mobs_7dtc.mobCurse:set_properties({
					state = "attack", 
					attack = mobs_7dtc.playerTarget,
				})
				mobs_7dtc.debug("recriado mob: "..minetest.pos_to_string(posPlayerTarget))
				--[[ ]]
				local msg = mobs_7dtc.getRandomWelcome(mobs_7dtc.playerTarget)
				if msg ~= "" then
					--minetest.log('action',"["..minetest.pos_to_string(posTarget).."] "..msg)
					minetest.log('action', "["..os.date("%Y-%m-%d %Hh:%Mm:%Ss").."] "..msg)
					minetest.chat_send_all(msg)
				end
				--]]
			end
		end
	else
		mobs_7dtc.debug("Ainda não está em neblina sangrenta!")
	end --fim de if mobs_7dtc.isNowFogTime() then
end

minetest.register_abm({ --Fonte: https://dev.minetest.net/minetest.register_abm
	label = "mobs_7dtc:curse",		
	nodenames = {
		"group:soil", 
		"group:dirt",
		"group:stone", 
		"group:sand",
							
		--"default:dirt_with_grass", 
		--"default:stone", 
		"nether:rack", 
		"nether:rack_deep"
	},
	neighbors = {"air"},
	interval = 30,
	chance = 90, --padrão: 5000
	catch_up = false,
	action = function(pos, node, active_object_count, active_object_count_wider)
		mobs_7dtc.on_spawn(pos, node, active_object_count, active_object_count_wider)
		return true --tentativa não testada de spawnar o Curse apenas uma única vez.
	end
})