
local S = mobs_7dtc.intllib


local curse_types = {
	--[[
	{	nodes = {"default:dirt_with_grass"}, --Onde vai Nascer
		skins = {"text_curse_grass.png"}, --Qual a skin vai tomar
	},
	--]]
	
	--[[
	{	nodes = {"nether:rack"},
		skins = {"mobs_dungeon_master_nether.png"},
	},
	{	nodes = {"nether:rack_deep"},
		skins = {"mobs_dungeon_master_netherdeep.png"},
	},
	--]]
}

mobs_7dtc.getNameTag = function()
	if (mobs_7dtc.display_spawn) then
		return S("CURSE")
	else
		return nil
	end
end


mobs:register_mob("mobs_7dtc:curse", {
	nametag = mobs_7dtc.getNameTag(),
	type = "monster",
	passive = false,
	--specific_attack = {"playername"}, --tem uma tabela de nomes de entidades que o mob também pode atacar. {"playername", "mobs_animal: frango"}. 
	damage = 6,
	knock_back = true, --quando true faz com que os mobs caiam para trás ao serem atingidos, quanto maior o dano, mais eles se movem para trás. Padrão: false.
	attack_type = "dogshoot",
	dogshoot_switch = 1,
	dogshoot_count_max = 12, -- shoot for 10 seconds
	dogshoot_count2_max = 3, -- dogfight for 3 seconds
	reach = 3, --é o quão longe o mob pode atacar o jogador quando estiver próximo, o padrão é 3 nós.
	shoot_interval = 3.2, --Padrão:2.2
	arrow = "mobs_7dtc:fireball",
	shoot_offset = 1,
	hp_min = 42,
	hp_max = 75,
	armor = 60,
	collisionbox = {-0.7, -1.8, -0.7, 0.7, 1.6, 0.7}, --Padrão: {-0.7, -1, -0.7, 0.7, 1.6, 0.7},
	visual = "mesh",
	mesh = "mesh_curse.b3d",
	textures = { --Pode haver mais de uma textura sorteada
		{"text_curse.png"},
	},
	blood_texture = "text_fireball.png",
	makes_footstep_sound = false, --padrão: true
	sounds = {
		random = "sfx_bruxa_rizada", --som aleatório 'aleatório' que é reproduzido durante o jogo.
		--war_cry = 'sfx_bruxa_rizada', --o que você ouve quando o mob começa a atacar o jogador ou quando .
		attack = 'default_punch', --o que você ouve ao ser atacado.
		shoot_attack = "sfx_fireball", --o que você ouve quando o mob atira.
		--jump = 'sfx_bruxa_rizada', --o que você ouve  quando o mob pula.
		damage = 'default_punch', --o que você ouve quando o mob sofre dano.
		death = 'sfx_bruxa_rizada', --o que você ouve quando o mob é morto.
		--fuse = '', --O som de 'fusível' é reproduzido quando o cronômetro de explosão do mob começa.
		--explode = '', --som tocado quando mob explode.
	},
	walk_velocity = 2, --Padrão: 1
	run_velocity = 6, --Padrão: 3
	jump = true,
	--fly = true,
	--fly_in = "air",
	view_range = 30,
	jump_height = 5,
	fall_damage = 0,
	fall_speed = -6,
	stepheight = 2.1, --altura do voo. Padrão: 2.1
	drops = {
		{name = "default:mese_crystal", chance = 3, min = 0, max = 2},
		{name = "default:diamond", chance = 4, min = 0, max = 1},
	},
	floats = 1, --quando definido como 1 mob irá flutuar na água, 0 faz com que afundem. 
	water_damage = 0,
	lava_damage = 1,
	light_damage = 10,
	fear_height = 10, --Medo de Altura. Padrão: 3
	
	animation = { --higor
		speed_normal = 30, --Velocidade da animação andando. padrão: 30
		speed_run = 1, --Velocidade da animação correndo. padrão: 30
		stand_start = 0, --padrão: 0
		stand_end = 80, --padrão: 70
		walk_start = 0, --padrão: 168
		walk_end = 80, --padrão: 187
		run_start = 194, --padrão: 168
		run_end = 194, --padrão: 187
		punch_start = 189, --padrão: 200
		punch_end = 199,  --padrão: 219
		
		--[[
			stand			inicio: 0,		fim: 80
			sit			inicio: 81,		fim: 161
			lay			inicio: 162,	fim: 167
			walk/run		inicio: 168,	fim: 188
			mine			inicio: 189,	fim: 199
			walk_mine	inicio: 200,	fim: 219
		--]]
	},
	
	--[[ --Desativado porque causa lag demais. fica injogavel pelo celular.
	do_custom = function(self, dtime) --Faz o nome do personagem aparecer se 7dtc_display_spawn = true
		--mobs_7dtc.debug(dtime.." nametag = '"..dump(mobs_7dtc.getNameTag()).."'")
		--minetest.chat_send_all(dtime.." nametag = '"..dump(mobs_7dtc.getNameTag()).."'")
		if self.nametag ~= mobs_7dtc.getNameTag() then
			self.nametag = mobs_7dtc.getNameTag()
		end
		--return false --Não retorn para não paralizar este mob de andar.
	end,
	--]]
	--on_rightclick = function(self, pos) --[[ ]]	end,
	on_die = function(self, pos)
		if self.attack~=nil and self.attack:is_player() then
			minetest.chat_send_all(
				core.colorize("#FF0000", "<"..S("Curse").."> ")
				..S(
					"I WILL BE BACK, @1 !!!",
					core.colorize("#00FF00", self.attack:get_player_name()) 
				)
			)
		end
	end,

	-- check surrounding nodes and spawn a specific monster
	--on_spawn = mobs_7dtc.on_spawn_obsoleto,
	--on_spawn = mobs_7dtc.on_spawn_v2,
	--[[  
	on_spawn = function(self)
		if self.state == "attack" 
			and self.attack~=nil 
			and self.attack:is_player() 
		then
			local msg = mobs_7dtc.getRandomMensagem(self.attack)
			if msg ~= "" then
				--minetest.log('action',"["..minetest.pos_to_string(posTarget).."] "..msg)
				minetest.log('action', "["..os.date("%Y-%m-%d %Hh:%Mm:%Ss").."] "..msg)
				minetest.chat_send_all(msg)
			end
		end
	end,
	--]]
})

--sintaxe: mobs:register_egg(name, description, background, addegg, no_creative)
--mobs:register_egg("mobs_7dtc:curse", S("Curse"), "icon_curse.png", 1)
mobs:register_egg("mobs_7dtc:curse", S("Curse"), "icon_curse.png", 0, false)

--SINTAXE: mobs:alias_mob("<genericname>", "<entityname>")
mobs:alias_mob("mobs:curse", "mobs_7dtc:curse") -- compatibility
mobs:alias_mob("curse", "mobs_7dtc:curse")
mobs:alias_mob("7dtc", "mobs_7dtc:curse")


-- fireball (weapon)
mobs:register_arrow("mobs_7dtc:fireball", {
	visual = "sprite",
	visual_size = {x = 1, y = 1},
	textures = {"icon_curse.png"},
	collisionbox = {-0.1, -0.1, -0.1, 0.1, 0.1, 0.1},
	velocity = 7,
	tail = 1,
	tail_texture = "text_fireball.png",
	tail_size = 10,
	glow = 15, --Padrão: 5
	expire = 0.1,

	on_activate = function(self, staticdata, dtime_s)
		-- make fireball indestructable
		self.object:set_armor_groups({immortal = 1, fleshy = 100})
	end,

	-- if player has a good weapon with 7+ damage it can deflect fireball
	on_punch = function(self, hitter, tflp, tool_capabilities, dir)

		if hitter and hitter:is_player() and tool_capabilities and dir then

			local damage = tool_capabilities.damage_groups and
				tool_capabilities.damage_groups.fleshy or 1

			local tmp = tflp / (tool_capabilities.full_punch_interval or 1.4)

			if damage > 6 and tmp < 4 then

				self.object:set_velocity({
					x = dir.x * self.velocity,
					y = dir.y * self.velocity,
					z = dir.z * self.velocity,
				})
			end
		end
	end,

	-- direct hit, no fire... just plenty of pain
	hit_player = function(self, player)
		player:punch(self.object, 1.0, {
			full_punch_interval = 1.0,
			damage_groups = {fleshy = 8},
		}, nil)
	end,

	hit_mob = function(self, player)
		player:punch(self.object, 1.0, {
			full_punch_interval = 1.0,
			damage_groups = {fleshy = 8},
		}, nil)
	end,

	-- node hit
	hit_node = function(self, pos, node)
		if(mobs_7dtc.damane_node) then
			mobs:boom(self, pos, 1)
		else
			mobs:safe_boom(self, pos, 1)
		end
	end
})

--minetest.override_item("default:obsidian", {on_blast = function() end})
