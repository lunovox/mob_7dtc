local S = mobs_7dtc.intllib

mobs_7dtc.getRemainingDays = function()
	local diaAtual = minetest.get_day_count()
	local weekday = diaAtual % 7
	local daysRemaining
	if weekday == 0 then
		daysRemaining = 0
	else
		daysRemaining = 7 - weekday
	end
	return daysRemaining
end

mobs_7dtc.isNowFogTime = function()
	local days = minetest.get_day_count()
	local daysRemaining = mobs_7dtc.getRemainingDays()
	local timeofday = minetest.get_timeofday() * 24
	if days >= 7 and ( --Only after the first week.
		(daysRemaining == 0 and timeofday >= 19 and timeofday <= 24) --Saturday 19h~00h
		or (daysRemaining == 6 and timeofday >= 0 and timeofday <= 5) --Sunday: 00h~05h
	) then
		return true
	end
	return false
end

mobs_7dtc.doSkipDay = function(pular)
	if type(tonumber(pular)) == "number" and tonumber(pular) >= 1 then
			--mobs_7dtc.debug("pulando = "..pular)
			for n = 1, tonumber(pular) do
				minetest.set_timeofday(0.0)
				minetest.set_timeofday(5/24)  --It goes to 05h in the morning, time to wake up.
			end
			local diaAtual = minetest.get_day_count()
			local daysRemaining = mobs_7dtc.getRemainingDays()
			--minetest.chat_send_player(playername, 
			minetest.chat_send_all(
				"[".. core.colorize("#00FFFF", "7DTC").."] "
				..S(
					"Now this is the day @1°.",
					string.format("%02d", diaAtual)
				).." "
				..S(
					"There are @1 days left for the Bloody Mist!!!", 
					core.colorize("#FFFF00",
						string.format("%02d",daysRemaining)
					)
				)
			)
			mobs_7dtc.update_cicle()
		return daysRemaining
	else
		return nil
	end

end

mobs_7dtc.update_fog = function(player)
	local altura, nuvens = 0, {}
	local playername = player:get_player_name()
	if mobs_7dtc.fog_enable and altura ~= player:get_pos().y-50 then
		altura = player:get_pos().y-50
		nuvens = {
			thickness=100, --Grossura das Nuvens. padrão: 16
			color={r=255, g=0, b=0, a=64}, --cor das nuvens
			ambient={r=255, g=0, b=0, a=64}, --cor do ambiente
			density=1.0, --Parte do ceu cobrrto de nuvel. Alcace:0.0~1.0, padrão: 0.4
			height=altura, --Altura das Nuvens. padrão: 100
			--speed={y=-2,x=-1},
		}
		if type(mobs_7dtc.bmg_wind[playername]) == "nil" then
			mobs_7dtc.bmg_wind[playername] = minetest.sound_play("bmg_ventania_q0", {
				loop = true, --Se o audio é tocado em loop. default = false
				gain = 1.0, --Ganho de volume. default = 1.0
				--pos = {x=0, y=0, z=0}, --O epicentro do audio é uma posição específica.
				--to_player = "playername" --O epicentro do audio é um jogador com um nome específico.
				--object = ObjectRef --O epicentro do audio é um objeto específico.
				--max_hear_distance = 100, --Raio de diatancia ouvida a partir do epicentro, Se não houver epcentro, essa parametro é desnecessário pois tocará para todos os jogadores do mapa. default = 32
			})
		end
		--player:set_sky({r=128, g=0, b=0},"regular", nuvens)
		player:set_clouds(nuvens)
		--mobs_7dtc.debug("player:set_clouds : nuvem baixa")
	elseif not mobs_7dtc.fog_enable and altura ~= 100 then
		altura = 100
		nuvens = {
			thickness=16, --Grossura das Nuvens. padrão: 16
			color={r=255, g=255, b=255, a=128}, --cor das nuvens
			ambient={r=255, g=255, b=255, a=255}, --cor do ambiente
			density=0.4, --Parte do ceu cobrrto de nuvel. Alcace:0.0~1.0, padrão: 0.4
			height=altura, --Altura das Nuvens. padrão: 100
			--speed={y=-2,x=-1},
		}
		if type(mobs_7dtc.bmg_wind[playername]) ~= "nil" then
			minetest.sound_stop(mobs_7dtc.bmg_wind[playername])
			mobs_7dtc.bmg_wind[playername] = nil
		end
		--player:set_sky({r=128, g=0, b=0},"regular", nuvens)
		player:set_clouds(nuvens)
		--mobs_7dtc.debug("player:set_clouds : nuvem alta")
	end
	--mobs_7dtc.debug("mobs_7dtc.update_fog("..playername..")")
end

mobs_7dtc.update_cicle = function()
	if mobs_7dtc.blood_fog and mobs_7dtc.isNowFogTime() then
		if mobs_7dtc.fog_enable ~= true then
			minetest.settings:set("viewing_range", 20) --Minimo de 20, Padrão: 50
			--Força o mod "lightning" a voltar a trovejar automaticamente
			mobs_7dtc.lightning.auto = true
			mobs_7dtc.lightning.strike()
		end
		mobs_7dtc.fog_enable = true
		for _, player in ipairs(minetest.get_connected_players()) do
			mobs_7dtc.update_fog(player)
		end
	else--if (minetest.get_day_count() < 7 or minetest.get_day_count() % 7 ~= 0) and mobs_7dtc.fog_enable ~= false then
		if mobs_7dtc.fog_enable ~= false then
	minetest.settings:set("viewing_range", mobs_7dtc.old_viewing_range)
			--Força o mod "lightning" a não trovejar automaticamente
			mobs_7dtc.lightning.auto = false
			mobs_7dtc.lightning.strike()
		end
		mobs_7dtc.fog_enable = false 
		for _, player in ipairs(minetest.get_connected_players()) do
			mobs_7dtc.update_fog(player)
		end
	end
	--mobs_7dtc.debug("NEBLINA SANGRENTA: dayRamaining("..mobs_7dtc.getRemainingDays()..") timeOfDay("..minetest.get_timeofday()..")")

end

mobs_7dtc.change_fog = function()
	mobs_7dtc.blood_fog = not (mobs_7dtc.blood_fog == true)
	minetest.settings:set_bool("7dtc_blood_fog", mobs_7dtc.blood_fog)
	if mobs_7dtc.blood_fog then
		local daysRemaining = mobs_7dtc.getRemainingDays()
		minetest.chat_send_all(
			"[".. core.colorize("#00FFFF", "7DTC").."] "..
			S(
				"The Bloody Mist Timer is now '@1' in the settings.",
				core.colorize(
					"#00FF00",
					 S("TRUE")
				)
			).." "
			..S(
			"There are @1 days left for the Bloody Mist!!!",
				core.colorize(
					"#FFFF00",
					 string.format("%02d",daysRemaining)
				)
			)
		)
	else
		minetest.chat_send_all(
			"[".. core.colorize("#00FFFF", "7DTC").."] "..
			S(
				"The Bloody Mist Timer is now '@1' in the settings.",
				core.colorize(
					"#FF0000",
					 S("FALSE")
				)
			)
		)
	end
	mobs_7dtc.update_cicle()
end

minetest.register_globalstep(function(dtime) 
	mobs_7dtc.timer = mobs_7dtc.timer + dtime; 
	if mobs_7dtc.timer >= 5 then 
		-- Send "Minetest" to all players every 5 seconds 
		--minetest.chat_send_all("Minetest") 
		mobs_7dtc.update_cicle()
		mobs_7dtc.timer = 0 
	end
end)

minetest.register_on_joinplayer(function(player)
	--mobs_7dtc.debug_fog(player)
	mobs_7dtc.update_cicle()
end)

minetest.register_on_leaveplayer(function(player) 
	--minetest.chat_send_all(player:get_player_name().." has left this awesome game.") 
	local playername = player:get_player_name()
	if type(mobs_7dtc.bmg_wind[playername]) ~= "nil" then
		minetest.sound_stop(mobs_7dtc.bmg_wind[playername])
		mobs_7dtc.bmg_wind[playername] = nil
	end
end) 

minetest.register_on_shutdown(function()
	minetest.settings:set("viewing_range", mobs_7dtc.old_viewing_range)
end) 
	

