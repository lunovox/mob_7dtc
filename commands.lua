local S = mobs_7dtc.intllib

minetest.register_chatcommand("changefog", {
	params = "",
	description = S(
		"Turns the Auto Bloody Mist on and off. "
		.."Need 'server' privilege."
	),
	func = function(playername, param)
		local isPriv = minetest.check_player_privs(playername, {server=true})
		if isPriv then
			mobs_7dtc.change_fog()
		else
			minetest.chat_send_player(
				playername, 
				"[".. core.colorize("#FF0000", "7DTC"..":"..S("FAILURE")).."] "
				..S(
					"You do not have the '@1' privilege to execute this command!!!", 
					core.colorize("#00FF00","server")
				)
			)
		end
	end,
})

-- skipped days
minetest.register_chatcommand("skipday", {
	params = "[number]",
	description = S(
		"Forces the calendar to skip a certain number of days. "
		.."Maximum number of 365 days. Need 'settime' privilege."
	),
	func = function(playername, param)
		local isPriv = minetest.check_player_privs(playername, {settime=true})
		if isPriv then
			local pular = 0
			--mobs_7dtc.debug("type('"..param.."') = "..type(param))
			if param == "" or tonumber(param) == nil or tonumber(param) < 1 then
				pular = 1
			elseif tonumber(param) <= 365 then
				pular = tonumber(param)
			else
				minetest.chat_send_player(
					playername, 
					"[".. core.colorize("#FF0000", "7DTC"..":"..S("FAILURE")).."] "
					..S(
						"The maximum number of days is exactly '@1'!!!", 
						core.colorize("#FF0000","365")
					)
				)
				return true --false  = quando o comando falha.
			end
			mobs_7dtc.doSkipDay(pular)
			return true --true=sucesso
		else
			minetest.chat_send_player(
				playername, 
				"[".. core.colorize("#FF0000", "7DTC"..":"..S("FAILURE")).."] "
				..S(
					"You do not have the '@1' privilege to execute this command!!!", 
					core.colorize("#00FF00","settime")
				)
			)
		end
	end,
})

minetest.register_chatcommand("fogwhen", {
	--params = "[number]",
	description = S(
		"Displays how many days are left for Bloody Mist."
	),
	func = function(playername, param)
		local da = minetest.get_day_count()
		local rd = mobs_7dtc.getRemainingDays()
		local fogNow = mobs_7dtc.isNowFogTime()
		
		if fogNow then
			minetest.chat_send_player(
				playername, 
				"[".. core.colorize("#00FFFF", "7DTC").."] "
				..S(
					"You are over the Blood Mist event right now!" 
				)
			)
		else
			if rd == 0 then
				minetest.chat_send_player(
					playername, 
					"[".. core.colorize("#00FFFF", "7DTC").."] "
					..S(
						"The Blood Mist event will take place today on sunny day!" 
					)
				)
			else
				minetest.chat_send_player(
					playername, 
					"[".. core.colorize("#00FFFF", "7DTC").."] "
					..S(
						"Now this is the day @1°.",
						string.format("%02d", da)
					).." "
					..S(
						"There are @1 days left for the Bloody Mist!!!", 
						core.colorize("#FFFF00",
							string.format("%02d",rd)
						)
					)
				)
			end
		end
	end,
})

