--[[  --Desativado porque causam luzes muito duras
minetest.settings:set("display_gamma", 0.05) --Padrão: 1.0
minetest.settings:set("ambient_occlusion_gamma", 0.05) --Projeção de sombra. Min: 0.25 | Max: 4.0 | Padrão: 1.8
minetest.settings:set("lighting_beta", 0.0) --Luz Mínima. Padrão: 1.0
minetest.settings:set("lighting_boost_center", 0.05) --Luz Média. Padrão: 0.5
minetest.settings:set("lighting_beta", 0.10) --Luz Maxima. Padrão: 1.5
--]]

mobs_7dtc.old_viewing_range = minetest.settings:get("viewing_range") or 50 --Padrão: 50
mobs_7dtc.damane_node = minetest.settings:get("7dtc_damane_node") ~= "false" --Default: true
mobs_7dtc.blood_fog = minetest.settings:get("7dtc_blood_fog") ~= "false" --Default: true
mobs_7dtc.display_spawn = minetest.settings:get_bool("7dtc_display_spawn") --Default: false


--configurações obrigatórias.
minetest.settings:set("mob_nospawn_range", 1.0)  --ou o Curse spawna muito longe.
minetest.settings:set_bool("enable_weather", false) --ou a neblina não funciona.
minetest.settings:set_bool("enable_3d_clouds", true) --ou a neblina não funciona.
minetest.settings:set_bool("7dtc_damane_node", mobs_7dtc.damane_node)
minetest.settings:set_bool("7dtc_blood_fog", mobs_7dtc.blood_fog)
minetest.settings:set_bool("7dtc_display_spawn", mobs_7dtc.display_spawn)

mobs_7dtc.debug = function(debugtext)
	if (mobs_7dtc.display_spawn) then
		minetest.chat_send_all(debugtext)
	end
end

--[[ --]]
if minetest.get_modpath("lightning") then
	mobs_7dtc.lightning = lightning --Importando AI de trovões
	mobs_7dtc.lightning.interval_low = 5 --Intervalo mínimo em sugundos entre relampagos. Padrão 17
	mobs_7dtc.lightning.interval_high = 20 --Intervalo máximo em sugundos entre relampagos. Padrão 503
	mobs_7dtc.lightning.range_h = 100 --Diametro da tempestade. Padrão 100. Imagino que se ficar muito distante não da para ouvir.
	mobs_7dtc.lightning.range_v = 50 --Autura da tempestada. Padrão 50
	mobs_7dtc.lightning.size = 100 --Autura da discarga eletrica. Padrão 100
	mobs_7dtc.lightning.auto = true --Força o mod "lightning" a não trovejar automaticamente
	mobs_7dtc.lightning.strike()
else
	mobs_7dtc.lightning = {
		auto = false,
		strike= function()
			mobs_7dtc.debug("[TDTC] Mod 'lightning' ausente. Impossivel fazer descarga elétrica. ")
		end
	}
end
--]]
