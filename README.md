[![lnk_thumb]][lnk_video]
<small>Click on this image to watch a teaser trailer!</small>

<video controls poster="https://gitlab.com/lunovox/mob_7dtc/-/raw/main/screenshot.png" autoplay=true>
  <source  src="https://gitlab.com/lunovox/mob_7dtc/-/raw/publisher/2022-05-16/published_video.mp4"   type="video/mp4">
</video>

# [MOB: 7 DAY TO CURSE]

**[Minetest Mod]** Adds the Curse mob. Once every 7 days, there will be a Blood Fog day. If the Curse encounters someone who walks in darkness during the Blood Mist, it will capture them and throw them into the Mouth of Nether. 

____

### **Dependencies:**
  * [Mobs Redo] → The Mob Engine

### **Optional Dependencies:**
  * [intllib] → Facilitates the translation of several other mods into your native language, or other languages.
  * [lightning] → Add lightning and thunder to the Bloody Mist event.
  * [Nether] → Adds nether biome, a type of lava hell in the depths of the world.
  
### **Licence:**
 * [GNU AGPL]

### **Developers:**
 * Lunovox Heavenfinder: [email](mailto:lunovox@disroot.org), [social web](http:mastodon.social/@lunovox), [webchat](https://cloud.disroot.org/call/9aa2t7ib), [xmpp](xmpp:lunovox@disroot.org?join), [Mumble](mumble:mumble.disroot.org), [more contacts](https:libreplanet.org/wiki/User:Lunovox)

----

### **SETTINGS**

These are information that are in the ```minetest.conf``` file.

#### Mandatory settings:

```lua
enable_3d_clouds = true
enable_weather = false
```

That indicates how the mod will work, and whether the mod will work. If configured incorrectly, the mod may either not work or it may work incorrectly.

```lua
7dtc_blood_fog = true
``` 

Turns the timing for the Blood Fog event on or off. Can be changed with the game already running via the ````/changefog```` command.


#### Automatic settings:

```lua
viewing_range = 50
```

These are settings that are constantly being changed automatically by the mod. This information can only be altered to make a difference in the game, if the game is closed beforehand.

#### Optional settings

You can change these settings through the game's home menu panel.

* ```7dtc_damane_node``` : Whether or not Curse can destroy the terrain. Default: True.
* ```7dtc_display_spawn``` : This feature is only used by programmers to debug code. Default: False

----

### **COMMANDS:**

#### by common players:

* ````/fogwhen```` : Displays how many days are left for Bloody Mist.

#### by admins:

* ````/changefog```` : Turns the Auto Bloody Mist on and off. If ON the automatic blood fog event will occur every 7 days. If OFF the event does not occur. This command only works if  ```7dtc_blood_fog = true``` in the settings. Need ````server```` privilege.

* ````/skipday [number]```` : Forces the calendar to skip a certain number of days. Maximum number of 365 days. Need ````settime```` privilege.

### Sound of blood fog event:
<audio controls autoplay>
  <source src="https://gitlab.com/lunovox/mob_7dtc/-/raw/main/sounds/bmg_ventania_q0.ogg" type="audio/ogg">
  Your browser does not support the audio element.
</audio>

<!--

### **Translate to Others Languages:**

* This mod currently are configured to language:
	* [English] (Default)
	* [Portuguese]

* To add a new language to this mod just follow the steps below:
	* Enable the complementary mod **'intllib'.**
	* Set your language in **'minetest.conf'** by adding the [````language = <your language>````] property. 
		* Example for French Language: ````language = fr````
	* Make a copy of the file [ **pt.txt** ] in the [ **locale** ] folder that is inside the mod for [````locale/<your_language>.txt````]. 
		* Example for French language: ````locale/fr.txt````
	* Open the file [````locale/<your_language>.txt````] in a simple text editor.
	* Translate all lines. But, just here that stems the right of the equals symbol (=). 
		* Example for French Language: ````Only the Owner = Seul le propriétaire````
	* Send your translated file to [lunovox@disroot.org](mailto:lunovox@disroot.org).

-->

----

### **WHAT REMAINS TO BE DEVELOPED?**

1. **Fix Curse spawn** : The Curse needs to spawn only on the player who is walking on poorly lit terrain, causing a jump scare.

2. **Integrate 'Lighting' mod into '7dtd' mod** : The light mod 'lightning' is using deprecated functions which are polluting `debug.txt`. An integration would resolve this missing dependency.

3. **Add the Hellmouth Time** : The player wandering around in the dark specifically on Sundays from 00:00 to 03:00 will be dragged to the Nether by the Curse. The player will be trapped in the Nether until he dies or until he finds a return portal.

4. **Monster Sounds during Hellmouth Time** : During the mouth of hell hours there will be monster sounds in the environment. This will announce that the spout from hell has begun.




[English]:#
[GNU AGPL]:https://pt.wikipedia.org/wiki/GNU_Affero_General_Public_License
[intllib]:https://github.com/minetest-mods/intllib
[lightning]:https://content.minetest.net/packages/sofar/lightning/
[lnk_thumb]:https://gitlab.com/lunovox/mob_7dtc/-/raw/publisher/2022-05-16/published_thumb.png
[lnk_video]:https://gitlab.com/lunovox/mob_7dtc/-/raw/publisher/2022-05-16/published_video.mp4
[MOB: 7 DAY TO CURSE]:https://gitlab.com/lunovox/mob_7dtc
[Mobs Redo]:https://notabug.org/TenPlus1/mobs_redo
[Nether]:https://github.com/minetest-mods/nether
[Portuguese]:#
[screenshot]:https://gitlab.com/lunovox/mob_7dtc/-/raw/main/screenshot.png

